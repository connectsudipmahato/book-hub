import React, { useState, useEffect } from "react";
import axios from "axios";
import "./book.css";

const Book = () => {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    //   the get request returns a promise
    axios
      .get(
        "https://api.nytimes.com/svc/books/v3/lists/current/hardcover-fiction.json?api-key=YMTZ8rRupvfx7Q3O2XsQg1KHZNIQeOSJ"
      )
      .then((res) => {
        setBooks(res.data.results.books);
        console.log(res.data.results.books);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <>
      <div>Book Hub</div>
      <section className="mainContainer">
        <div>
          {books.map((book) => {
            const {
              author,
              title,
              book_image,
              primary_isbn10,
              publisher,
              description,
              rank,
              contributor,
              buy_links,
              book_uri,
            } = book;

            return (
              <div className="bookContainer" key={rank}>
                <div>
                  <img src={book_image} alt={title} />

                  <div>
                    <h2>{title}</h2>
                    <p>{author}</p>
                    <p>{description}</p>
                  </div>
                  <ul>
                    <li>{publisher}</li>
                    <li>{primary_isbn10}</li>
                    <li>{contributor}</li>
                    <li>{book_uri}</li>
                  </ul>
                  <ul>
                    <p>Buy Now:</p>
                    {buy_links.map((link) => {
                      const { name, url } = link;
                      return (
                        <div key={name}>
                          <a href={url}>{name}</a>
                        </div>
                      );
                    })}
                  </ul>
                </div>
              </div>
            );
          })}
        </div>
      </section>
    </>
  );
};

export default Book;
